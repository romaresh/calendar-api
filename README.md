# calendar-api

Test task for RT Projects

## Features

- For Django 2.1 and Python 3.7
- Modern virtual environments with [pipenv](https://github.com/pypa/pipenv)
- Custom user model
- Email/password for log in/sign up instead of Django's default username/email/password pattern
- Celery + [RabbitMQ](https://www.vultr.com/docs/how-to-install-rabbitmq-on-ubuntu-16-04-47) for background tasks 

## First-time setup

1)  Make sure Python 3.7x, RabbitMQ and Pipenv are already installed.

2)  Clone the repo and configure the virtualenv:

```
$ git clone https://romaresh@bitbucket.org/romaresh/calendar-api.git
$ cd calendar-api
$ pipenv install
$ pipenv shell
```

3.Create `config.json` file and put it to `calendar-api/calendar_api/settings/`. File shoud contain the following fields:
```json
{
  "SECRET_KEY": "secretkey123",
  "BROKER_URL": "rabbit url here",
  "CELERY_RESULT_BACKEND": "rabbit url here",
  "EMAIL_HOST": "smtp.gmail.com",
  "EMAIL_PORT": 587,
  "EMAIL_HOST_USER": "mail@mail.ru",
  "EMAIL_HOST_PASSWORD": "password"
}

```

4.Set up the initial migration for our custom user models in `users` and build the database.

```
(calendar-api) $ python manage.py makemigrations users
(calendar-api) $ python manage.py makemigrations countries
(calendar-api) $ python manage.py makemigrations events
(calendar-api) $ python manage.py migrate
```

5.Create a superuser:

```
(calendar-api) $ python manage.py createsuperuser
```

6.Confirm everything is working:

```
(calendar-api) $ python manage.py runserver
```
and in separate termianl start celery:
```
(calendar-api) $ ./celery.sh
```

Load the site at [http://127.0.0.1:8000](http://127.0.0.1:8000).
