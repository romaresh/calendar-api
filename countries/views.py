import calendar
from datetime import datetime

from rest_framework import generics
from rest_framework.decorators import api_view, permission_classes
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response

from .models import Country
from .serializers import HolidaySerializer, CountrySerializer


class CountryListView(generics.ListAPIView):
    queryset = Country.objects.all()
    serializer_class = CountrySerializer


@api_view(['POST'])
@permission_classes((IsAuthenticated, ))
def get_holidays(request):
    """
    Return holidays for country that user specified in profile.
    By default returned holidays for current month and year.
    Month in body may specified like string(f.e. September) or
    like number.
    """
    month = request.data.get('month', datetime.now().month)
    year = request.data.get('year', datetime.now().year)
    if isinstance(month, str) and month.isdigit() and int(month) in range(1, 13):
        month = int(month)
    elif isinstance(month, str):
        try:
            month = list(calendar.month_abbr).index(month.lower().capitalize()[:3])
        except Exception:
            return Response({'details': 'Not valid params.'})
    if request.user.country:
        holidays = request.user.country.holidays.filter(begin__month=month, begin__year=year)
        serializer = HolidaySerializer(holidays, many=True)
        return Response({"holidays": serializer.data})
    else:
        return Response({"details": "You didn't specify a country at your profile."})

