from django.contrib import admin
from .models import Country, Holiday

admin.site.register(Country)
admin.site.register(Holiday)
