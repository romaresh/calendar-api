from django.urls import path

from . import views

urlpatterns = [
    path('countries/', views.CountryListView.as_view()),
    path('holidays/', views.get_holidays),
]
