import requests
from arrow.parser import ParserError
from django.db import models
from ics import Calendar


class Country(models.Model):
    name = models.CharField(max_length=100, blank=False, unique=True)
    created = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return f"{self.name}"


class Holiday(models.Model):
    name = models.CharField(max_length=200, blank=False)
    description = models.CharField(max_length=1000, blank=True)
    begin = models.DateTimeField(blank=False)
    end = models.DateTimeField(blank=False)
    country = models.ForeignKey(Country, related_name='holidays', on_delete=models.CASCADE)
    created = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return f"Holiday({self.name})"


def get_holidays_for_country(country):
    if not country.holidays.count():
        ICS_COUNTRY_CALENDAR_URL = "https://www.officeholidays.com/ics/ics_country.php?tbl_country={country}"
        try:
            c = Calendar(requests.get(ICS_COUNTRY_CALENDAR_URL.format(country=country.name)).text)
        except ParserError as ex:
            print(f"No such country '{country.name}' at www.officeholidays.com")
        else:
            holidays = []
            for event in c.events:
                holidays.append(
                    Holiday(name=event.name.split(':', maxsplit=1)[1],
                            description=event.description.split('\n', maxsplit=1)[0],
                            begin=str(event.begin), end=str(event.end),
                            country=country))
            Holiday.objects.bulk_create(holidays)
