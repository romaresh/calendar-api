from .base import *
import json
from django.core.exceptions import ImproperlyConfigured

with open('calendar_api/settings/config.json') as f:
    configs = json.loads(f.read())


def get_env_var(setting, configs=configs):
    try:
        val = configs[setting]
        if val == 'True':
            val = True
        elif val == 'False':
            val = False
        return val
    except KeyError:
        error_msg = "ImproperlyConfigured: Set {0} environment variable".format(setting)
        raise ImproperlyConfigured(error_msg)


# get secret key
SECRET_KEY = get_env_var("SECRET_KEY")

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

ALLOWED_HOSTS = []

INSTALLED_APPS += [
    'rest_framework',
    'rest_framework.authtoken',
    'rest_auth',
    'users',
    'countries',
    'events',
    'rest_framework_swagger',
]

AUTH_USER_MODEL = 'users.CustomUser'

REST_FRAMEWORK = {
    'DEFAULT_AUTHENTICATION_CLASSES': (
        'rest_framework.authentication.TokenAuthentication',
        # 'rest_framework.authentication.SessionAuthentication'
    ),
    'DEFAULT_PERMISSION_CLASSES': (
        'rest_framework.permissions.AllowAny',
    )
}


# Rabbit
BROKER_URL = get_env_var("BROKER_URL")
CELERY_RESULT_BACKEND = get_env_var("CELERY_RESULT_BACKEND")




# Email
EMAIL_HOST = get_env_var("EMAIL_HOST")
EMAIL_PORT = get_env_var("EMAIL_PORT")
EMAIL_HOST_USER = get_env_var("EMAIL_HOST_USER")
EMAIL_HOST_PASSWORD = get_env_var("EMAIL_HOST_PASSWORD")
EMAIL_USE_TLS = True

