import os
from . import settings
from celery import Celery
from celery.schedules import crontab

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'calendar_api.settings')
app = Celery('calendar_api')

app.config_from_object('django.conf:settings')
app.autodiscover_tasks(lambda: settings.INSTALLED_APPS)


@app.on_after_configure.connect
def setup_periodic_tasks(sender, **kwargs):
    # Executes every Monday morning at 3:30 a.m.
    sender.add_periodic_task(
        crontab(hour=3, minute=30, day_of_week=1),
        update_holidays.s(),
    )

