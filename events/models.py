from datetime import datetime

from django.db import models

from users.models import CustomUser

REMINDER_TIME_CHOICES = (
    ('', '----'),
    ('1 hours', '1 hours'),
    ('2 hours', '2 hours'),
    ('4 hours', '4 hours'),
    ('1 days', '1 days'),
    ('1 weeks', '1 weeks'),
)


class Event(models.Model):
    name = models.CharField(max_length=100, blank=False, unique=True)
    begin = models.DateTimeField(blank=False)
    end = models.DateTimeField(blank=False, default=datetime.now().replace(hour=23, minute=59, second=59))
    reminder_time = models.CharField(max_length=10, choices=REMINDER_TIME_CHOICES, default='')
    user = models.ForeignKey(CustomUser, related_name='events', on_delete=models.CASCADE)
    created = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return f"{self.name}"
