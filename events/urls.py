from django.urls import path

from . import views

urlpatterns = [
    path('event/', views.EventCreateView.as_view()),
    path('event/<int:pk>', views.EventDetailView.as_view(), name="event_detail"),
    path('events/', views.get_events),
]

