# Generated by Django 2.1.5 on 2019-01-08 18:32

import datetime
from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Event',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=100, unique=True)),
                ('begin', models.DateTimeField()),
                ('end', models.DateTimeField(default=datetime.datetime(2019, 1, 8, 23, 59, 59, 843994))),
                ('reminder_time', models.CharField(choices=[('', '----'), ('1 hours', '1 hours'), ('2 hours', '2 hours'), ('4 hours', '4 hours'), ('1 days', '1 days'), ('1 weeks', '1 weeks')], default='', max_length=10)),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('user', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='events', to=settings.AUTH_USER_MODEL)),
            ],
        ),
    ]
