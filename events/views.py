from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework import generics
from rest_framework.decorators import api_view, permission_classes

from .models import Event
from .serializers import EventSerializer
from datetime import datetime


class EventCreateView(generics.CreateAPIView):
    serializer_class = EventSerializer
    permission_classes = (IsAuthenticated,)

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)


class EventDetailView(generics.RetrieveUpdateDestroyAPIView):
    queryset = Event.objects.all()
    serializer_class = EventSerializer
    permission_classes = (IsAuthenticated,)


@api_view(['GET', 'POST'])
@permission_classes((IsAuthenticated,))
def get_events(request):
    """
    Return events for user. By default returned events for today but if specified
    field <period> in body with value <month> than return all events for current month.
    """
    period = request.data.get('period')    # default period - day
    if period == 'month':
        events = request.user.events.filter(begin__month=datetime.now().month)
    else:
        events = request.user.events.filter(begin__day=datetime.now().day)
    serializer = EventSerializer(events, many=True)
    return Response({"events": serializer.data})
