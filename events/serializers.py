from datetime import timedelta

from django.db.models.signals import pre_save
from django.dispatch import receiver
from rest_framework import serializers

from . import tasks
from .models import Event


class EventSerializer(serializers.ModelSerializer):
    user = serializers.ReadOnlyField(source='user.email')

    class Meta:
        model = Event
        fields = ('name', 'begin', 'end', 'reminder_time', 'user')


@receiver(pre_save, sender=Event)
def create_async_task(sender, instance=None, created=False, **kwargs):
    if instance.reminder_time:
        count, period = instance.reminder_time.split()
        begin = instance.begin
        args = {period: int(count)}
        execute_at = begin - timedelta(**args)
        serializer = EventSerializer(instance=instance)
        tasks.send_email_notification.apply_async((serializer.data,), eta=execute_at)
