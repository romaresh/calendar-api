from django.contrib import admin
from .models import Event


class EventAdmin(admin.ModelAdmin):
    model = Event
    list_display = ['name', 'user']
    search_fields = ('name',)
    ordering = ('user',)


admin.site.register(Event, EventAdmin)
