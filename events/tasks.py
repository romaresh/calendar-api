from celery import task
from django.core.mail import send_mail


@task
def send_email_notification(event):
    subject = 'Reminder from calendar-api'
    message = f"Reminder about {event.get('name')} which begin at {event.get('begin')} " \
              f"and finish at {event.get('begin')}."
    send_mail(
        subject,
        message,
        'no-reply@gmail.com',
        [event.get('user')],
        fail_silently=False,
    )
    return f"Reminder {event.get('name')} was sent to {event.get('user')}"

