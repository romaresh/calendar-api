from rest_framework import serializers

from . import models


class UserSerializer(serializers.ModelSerializer):
    country = serializers.ReadOnlyField(source='country.name')
    events = serializers.HyperlinkedRelatedField(many=True, read_only=True, view_name='event_detail')

    class Meta:
        model = models.CustomUser
        fields = ('country', 'email', 'events')
        write_only_fields = ('password',)
        read_only_fields = ('id',)
