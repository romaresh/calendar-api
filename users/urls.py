from django.urls import include, path

from . import views

urlpatterns = [
    path('rest-auth/register/', views.RegisterUsersView.as_view(), name="auth-register"),
    path('rest-auth/', include('rest_auth.urls')),
    path('user/', views.UserDetailView.as_view()),
]
