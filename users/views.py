from rest_framework import generics, permissions, status
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response

from .models import CustomUser
from .serializers import UserSerializer


class UserDetailView(generics.RetrieveUpdateDestroyAPIView):
    queryset = CustomUser.objects.all()
    serializer_class = UserSerializer
    permission_classes = (IsAuthenticated,)

    def get_object(self):
        return self.request.user


class RegisterUsersView(generics.CreateAPIView):
    """
    POST auth/register/
    """
    permission_classes = (permissions.AllowAny,)
    serializer_class = UserSerializer

    def post(self, request, *args, **kwargs):
        email = request.data.get("email", "")
        password = request.data.get("password", "")
        country = request.data.get("country", None)
        if not password and not email:
            return Response(
                data={
                    "message": "password and email is required to register a user"
                },
                status=status.HTTP_400_BAD_REQUEST
            )
        user = CustomUser.objects.create_user(
            password=password, email=email, country_name=country
        )
        serializer = UserSerializer(instance=user)
        return Response(status=status.HTTP_201_CREATED, data=serializer.data)
