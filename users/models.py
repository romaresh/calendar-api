from django.conf import settings
from django.contrib.auth.models import AbstractUser, BaseUserManager
from django.db import models
from django.db.models.signals import post_save
from django.dispatch import receiver
from rest_framework.authtoken.models import Token

from countries.models import Country, get_holidays_for_country


class CustomUserManager(BaseUserManager):
    def create_user(self, email, password=None, country_name=None):
        """
        Creates and saves a User with the given email, country and password.
        """
        if not email:
            raise ValueError('Users must have an email address')

        # normalize country name
        country_name = country_name.lower().capitalize() if country_name else None
        if not country_name:
            user = self.model(email=self.normalize_email(email))
        else:
            country = Country.objects.filter(name=country_name).first()
            if not country:
                country = Country(name=country_name)
                country.save()
            user = self.model(
                email=self.normalize_email(email),
                country=country
            )

        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_superuser(self, email, password):
        """
        Creates and saves a superuser with the given email and password.
        """
        user = self.create_user(
            email,
            password=password
        )
        user.is_admin = True
        user.is_staff = True
        user.is_superuser = True
        user.save(using=self._db)
        return user


class CustomUser(AbstractUser):
    username = None
    country = models.ForeignKey(Country, related_name='users', on_delete=models.CASCADE, null=True)
    email = models.EmailField(
        verbose_name='email address',
        max_length=255,
        unique=True,
    )

    objects = CustomUserManager()

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = []

    def __str__(self):
        return f"User{self.email}"


@receiver(post_save, sender=settings.AUTH_USER_MODEL)
def create_auth_token(sender, instance=None, created=False, **kwargs):
    """
    After saving user if user specified country trying fetch holidays
    for this country from external service. Also generate tauth token for new user.
    """
    if created:
        if instance.country:
            get_holidays_for_country(instance.country)
        Token.objects.create(user=instance)
